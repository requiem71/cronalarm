# CronAlarm

This module offers the posibility 
 * to create planned calls of custom functions
 * to create cron job
 * can synchronize with compute time (cron can start at hh:mm:00 to be sync with each minute)
 * specify the granularity of timing events 

This module use momentjs to process date/time.

This module can be used to trigger event at a specific date/time (like alarm clock), execute cron task each X seconds, including a start date.

It will be enhanced periodically.

## installation

`npm install cronalarm`

## Usage

To use CronAlarm, you need to instanciate a cron object :

```javascript
const cron  = require('cronalarm');

let cr = new cron({verbose : true, delay : 30, sync : false}) ;
```

When you create new object, you can specify some options :

 * verbose : boolean, default value : false.Indicates if CronAlarm need to display some debug information in console. In production mode, you can ignore verbose parameter,
 * sync : boolean, default value : false. Indicates if CronAlarm needs to synchronize with 00 seconds of system clock (see the section below about this point),
 * delay : this is the period of the timer used to trigger event. The interest of specifying this value is to not overload system with 1s tick if it's not necessary (see section below for more explaination.

Once you have instanciate CronAlarm, you can use it.

You can add two different types of job :
 * task : it's a job to do at a specific date, like alarm on alarmclock or event in a planning,
 * cron : it's a periodic job to do that starts at a specific date and will be triggered periodically depending on the delay.

### Task

A task is a single job to execute at a specific date, <u>__only one time__</u>
It's like a alarm in a alarm clock or an event in your smartphone calendar.


To add a task in CronAlarm, you need to call
```javascript
cr.setTask(<settigns>);
```

where settings is an object :

```javascript
{
  timeout : { 			 // Add a delay to the start date (option)
    delay : integer,		 // delay
    unit : string 		 // unit of the delay (like moment unit)
  },
  date : "YYYY-MM-DD HH:mm:ss",  // date when the event must be triggered (option)
  callback : myCallBack ,        // method  / functions to call when event occurs
  params : {},                   // object of parameters to send to callback
  id : "myIdString"              // id of the task
}
```

Example :

```javascript
const cron  = require('cronalarm');

let cr = new cron({delay : 15, sync : false}) ;

cr.setTask(
  {
    date : "2018-10-12 13:25:30",
    callback : myCallBack,
    params : {a : 10, b: 3, c:"test CallBack" },
    id : "myIdString"
  }
);

function myCallBack(params) {
  console.log(params.c);
  console.log("adding a+b = "+(params.a+params.b));
}
```

When CronAlarm will detect that the task date is older than or equal to the system date, the task is fired, the task is executed and post-deleted from the list of task. By this way, the task is a single task execution

If you want to delete a task that is not yet executed, because you considere that the task have now no interest to be executed, you can delete the task by passing in parameter the id (string) that you specified in the setTask :

```javascript

...
cr.setTask(
  {
    date : "2018-10-12 13:25:30",
    callback : myCallBack,
    params : {a : 10, b: 3, c:"test CallBack" },
    id : "myIdString"		// <== reference for deleting
  }
);

...

cr.delTask("myIdString");

```

### Cron

A Cron job is a periodic task that you want to execute each N seconds/minutes/hours, and it can start at a specific date.

To add a Cron in CronAlarm, you need to call
```javascript
cr.setCron(<settigns>);
```

where settings is an object :

```javascript
{
  delay : 10,			 // delay in seconds to periodicaly trigger the callback
  date : "YYYY-MM-DD HH:mm:ss",  // date when the event must be triggered (option)
  callback : myCallBack ,        // method  / functions to call when event occurs
  params : {},                   // object of parameters to send to callback
  id : "myIdString"              // id of the task
  run : true			 // start / stop cron (option)
}
```

Example :

```javascript
const cron  = require('cronalarm');

let cr = new cron({delay : 15, sync : false}) ;

cr.setCron(
  {
    delay : 60,
    date : "2018-10-12 13:25:30",
    callback : myCallBack,
    params : {a : 10, b: 3, c:"test CallBack" },
    id : "myIdString"
  }
);

/*
 this callback will be call each 60s from the 2018-10-12 at 13:25:30
*/

function myCallBack(params) {
  console.log(params.c);
  console.log("adding a+b = "+(params.a+params.b));
}
```

When CronAlarm will detect that the Cron date is older than or equal to the system date, the cron is fired, the callback is executed each "delay" seconds. 

If you want to delete a cron, because you considere that the cron have now no interest to be executed, you can delete it by passing in parameter the id (string) that you specified in the setTask :

```javascript

...
cr.setCron(
  {
    delay : 30,
    date : "2018-10-12 13:25:30",
    callback : myCallBack,
    params : {a : 10, b: 3, c:"test CallBack" },
    id : "myIdString"		// <== reference for deleting
  }
);

...

cr.delCron("myIdString");

```


You have the possibility to suspend / restart a specific Cron job.

To stop a cron, you can call the stopCron function :

```javascript

...
cr.setCron(
  {
    delay : 30,
    date : "2018-10-12 13:25:30",
    callback : myCallBack,
    params : {a : 10, b: 3, c:"test CallBack" },
    id : "myIdString"		// <== reference for deleting
  }
);

...

cr.stopCron("myIdString");
```

The cron is suspended and the callback will not be executed.

if you want to restart the Cron job, you can call the startCron function

```javascript

...
cr.setCron(
  {
    delay : 30,
    date : "2018-10-12 13:25:30",
    callback : myCallBack,
    params : {a : 10, b: 3, c:"test CallBack" },
    id : "myIdString"		// <== reference for deleting
  }
);

...

cr.stopCron("myIdString");

...

cr.startCron("myIdString");
```

## Synchronization with system clock

When you instanciate CronAlarm, you can specify in parameters that CronAlarm will or not wait to be synchronized with the system clock.

It means that CronAlarm wait the next full minutes, wait HH:mm:00 time, to start it's job.

It can be useful in certain cases, for example, if you absolutely need to execute cron at 00, 15, 30, 45 second of each minutes.

To synchronized with system clock, the parameters `sync` will be set to `true`. Otherwise, you can ommit it in the parameters list.

The synchronization must be set if you need exact absolute time to execute job. In this case, you need to wait the a certain time before your cron really start

If you just need to do jobs each X seconds, and not be precise on start date, the synchronization can be omitted.

## periodic delay (time granularity)

CronAlarm needs that you precise the delay on which the periodic process of Task/Cron Job will be done.

It's an __Important__ parameters, because it can have an impact on the reactivity of your application.

This parameter depend on the smallest delay for cron or the datetime to start tasks/crons.

The granularity must be set to your minimum delay Cron and must be a divider of this cron delay.

If you need a start date, the granularity must be set according to the 'seconds' of the date and the syncronization.

## case studies of Synchronization and interval of processing (granularity)

see example below :  (we don't show the callback definition, not important for explaination)

### 1 Cron : good settings

```javascript
const cron  = require('cronalarm');

let cr = new cron({delay : 15, sync : true}) ;

cr.setCron(
  {
    delay : 60,
    date : "2018-10-12 13:25:30",
    callback : myCallBack,
    params : {a : 10, b: 3, c:"test CallBack" },
    id : "myIdString"
  }
);
```

This code will works well because :
 * the interval of CronAlarm process is set to 15s,
 * the delay of cron is greater (60s) than the process interval,
 * the date is set to 13:25:30, 30s is a multiple of 15,
 * and CronAlarm is synchronize on system clock.

So, when the system clock will be exactly 13:25:30, the cron will start, and due to the interval of 60s it will be executed exactly on each HH:mm:30

### 1 Cron : bad settings

```javascript
const cron  = require('cronalarm');

let cr = new cron({delay : 20, sync : false}) ;

cr.setCron(
  {
    delay : 60,
    date : "2018-10-12 13:25:30",
    callback : myCallBack,
    params : {a : 10, b: 3, c:"test CallBack" },
    id : "myIdString"
  }
);
```

This code will works but the execution will not be at exact because :
 * the interval of CronAlarm process is set to 20s,
 * the delay of cron is greater (60s) than the process interval and it's a multiple of 20 (that's OK)
 * the date is set to 13:25:30, 30s is not a multiple of 20,
 * and CronAlarm is not synchronize on system clock.

So if we analyse the problem :

 * CronAlarm is not synchronized to system clock and the granularity it set to 20s, if you execute your application at 13:24:17, the date to start the cron job will not be exact : first tick is 13:24:17, next 13:24:37: next 13:24:57, next 13:25:17, next 13:25:37. The cron will start at 13:25:37 and not 13:25:30.
 * dur to delta time start (7s), the cron set to each 60 will be executed each HH:mm:37, SO if you want that cron will be executed each HH:mm:30.

### 2 Crons : good settings

```javascript
const cron  = require('cronalarm');

let cr = new cron({delay : 15, sync : true}) ;

cr.setCron(
  {
    delay : 60,
    date : "2018-10-12 13:25:30",
    callback : myCallBack,
    params : {a : 10, b: 3, c:"test CallBack1" },
    id : "myIdString1"
  }
);

cr.setCron(
  {
    delay : 30,
    date : "2018-10-12 13:25:45",
    callback : myCallBack,
    params : {a : 10, b: 3, c:"test CallBack2" },
    id : "myIdString2"
  }
);

```

This code will works well because :
 * the interval of CronAlarm process is set to 15s,
 * the delay of crons is greater (60s and 30s) than the process interval,
 * the dates are set to 13:25:30 and 13:25:45, 30s and 60s is a multiple of 15,
 * and CronAlarm is synchronize on system clock.


