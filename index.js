
let moment = require('moment') ;



/*
  settings
  {
    verbose : true,     // verbose mode for debug, default false
    delay   : integer,  // minimum timer granularity, default 60s, depends on the smallest timing you need
    sync    : true,     // specify if it's necessary to wait seconds to 00 before starting timer
  }



 */
function CronAlarm (settings) {
    this.TaskList = [] ;    // list of all single task to execute
    this.CronList = [] ;    // list of all periodic task to do
    // analyze Settings
    this.verbConsole = false ;
    if (settings.verbose) {
        this.verbConsole = settings.verbose ;
    }
    this.granularity = 60 ;
    if (settings.delay) {
        if ( (settings.delay>0) && (settings.delay<=60) ){
            this.granularity = settings.delay;
        }
    }


    // init timer to be synchronize with local time (seconds)

    if (settings.sync) {
        this.resyncTimer();
    } else {
        let _self = this ;
        setInterval(function () {_self.doJob(_self);}, _self.granularity*1000);
        this.verb("No synchro, start immediately");
    }

}


CronAlarm.prototype.resyncTimer = function() {
    let s = moment().seconds() ;
    this.verb(s);
    let self = this ;
    setTimeout(function () {self.synchroStart(self);}, (60-s)*1000) ;
    this.verb("Waiting for synchronization in "+(60-s)+" seconds");
};

CronAlarm.prototype.verb = function (msg) {
    if (this.verbConsole)
        console.log("CronAlarm "+moment().format("YYYY-MM-DD HH:mm:ss")+" - "+msg);
};

CronAlarm.prototype.synchroStart = function (_self) {
    setInterval(function () {_self.doJob(_self);}, _self.granularity*1000);
    _self.verb("synchronized");
    _self.doJobs(_self);

};

CronAlarm.prototype.doJob = function(_self) {
    _self.verb("Do Job now");
    _self.DoTasks(_self);
    _self.DoCrons(_self);
};

CronAlarm.prototype.DoTasks = function(_this) {
    let i = 0 ;
    let del = [] ;
    let self = _this ;
    self.TaskList.forEach(function (elt) {
        if (moment(elt.date)<=moment().milliseconds(0)) {
            elt.callback(elt.params, elt.id);
            del.push(i);
        }
        i++ ;
    });
    del.forEach( function(elt) {
        self.TaskList.splice(elt,1);
    });
};

/*
  {
    timeout : { delay : integer, unit : string }             // delay from date or current date in seconds
    date : YYYY-MM-DD HH:mm:ss  // date when the event must be triggered (option)
    callback :                  // method  / functions to call when event occurs
    params : {}                 // object of parameters to send to callback
    id : string                 // id of the task
  }
 */
CronAlarm.prototype.setTask = function(obj) {
    if (obj.date ===undefined) {
      obj["date"] = moment().milliseconds(0);
    }
    if (obj.timeout !== undefined) {
      obj["date"] = moment(obj.date,"YYYY-MM-DD HH:mm:ss").add(obj.timeout.delay, obj.timeout.unit).format("YYYY-MM-DD HH:mm:ss");
    }
    console.log(obj.date);
    this.TaskList.push(obj);
};

CronAlarm.prototype.delTask = function (id ) {
    let i = 0 ;
    let self = this ;
    this.TaskList.forEach(function (elt) {
        if (id === elt.id) {
            self.TaskList.splice(i,1);
            return ;
        }
        i++ ;
    });
};


/*
  {
    delay : integer             // delay to periodicaly fire the event
    date : YYYY-MM-DD HH:mm:ss  // date when the event must start. (option)
    callback :                  // method  / functions to call when event occurs
    params : {}                 // object of parameters to send to callback
    id : string                 // id of the task
    run : true                  // enable/disable the cron (option)
  }
 */
CronAlarm.prototype.setCron = function (obj)  {
    if (obj.run ===undefined) {
        obj["run"] = true ;                 // add "run" property
    }
    if (obj.date ===undefined) {
      obj["date"] = moment().milliseconds(0);
    }
    this.CronList.push(obj);
};

CronAlarm.prototype.delCron = function (id) {
    let i = 0 ;
    let self = this ;
    this.CronList.forEach(function (elt) {
        if (id === elt.id) {
            self.CronList.splice(i,1);
            return ;
        }
        i++ ;
    });
};

CronAlarm.prototype.stopCron = function (id) {
    let i = 0 ;
    let self = this ;
    this.CronList.forEach(function (elt) {
        if (id === elt.id) {
            self.CronList[i].run = false;
            return ;
        }
        i++ ;
    });
};

CronAlarm.prototype.startCron = function (id) {
    let i = 0 ;
    let self = this ;
    this.CronList.forEach(function (elt) {
        if (id === elt.id) {
            self.CronList[i].run = true;
            return ;
        }
        i++ ;
    });
};

CronAlarm.prototype.DoCrons = function(_this) {
    let i = 0 ;
    let self = _this ;
    _this.CronList.forEach(function (elt) {
        if (elt.run) {
            if (moment(elt.date) <= moment().milliseconds(0)) {
                self.CronList[i].date = moment().add(elt.delay,'s').format("YYYY-MM-DD HH:mm:ss");
                elt.callback(elt.params, elt.id);
            }
        }
        i++ ;
    });
};


module.exports = CronAlarm;
